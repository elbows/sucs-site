{* Banana viewer template, classic style *}

{if $mode == "detail"}
	<div class="box">
	<div class="boxhead"><h2>Banana Details for {$who}</h2></div>
	<div class="boxcontent">
	
	{if $awards|@count < 1}
		<p>{$who} doesn't have any bananas yet</p>
	{else}
		<table cellpadding="10">
		<tr>
			<th colspan="2">Bananas</th>
			<th>When</th>
			<th>By whom</th>
			<th>Why</th>
		</tr>
		{foreach name=awards from=$awards key=key item=award}
			<tr>
				<td>{$award.score}</td>
				<td>{bananaprint score=$award.score}</td>
				<td>{$award.whn|date_format:"%d %b %Y %R"}</td>
				<td>{$award.who}</td>
				<td>{$award.why}</td>
			</tr>
		{/foreach}
		</table>
	{/if}
	</div>
	<div class="hollowfoot"><div><div></div></div></div>
	</div>
{/if}

