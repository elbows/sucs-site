
<div class="cbb">
<h3>This year's leaderboard<img class="emblem" src="{$baseurl}/images/bananas/banana-bunch.png" alt="Yellow Bananas" /></h3>
<ol>
{foreach name=top from=$stats.yeartop key=key item=member}
	{if $member.real == TRUE} 
		<li><a href="{$baseurl}/Community/Members/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{else}
		<li><a href="{$baseurl}/Community/Bananas/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{/if}
{/foreach}
</ol>
</div>


<div class="cbb">
<h3>Most valued members<img class="emblem" src="{$baseurl}/images/bananas/banana-bunch.png" alt="Yellow Bananas" /></h3>
<ol>
{foreach name=top from=$stats.top key=key item=member}
	{if $member.real == TRUE} 
		<li><a href="{$baseurl}/Community/Members/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{else}
		<li><a href="{$baseurl}/Community/Bananas/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{/if}
{/foreach}
</ol>
</div>

<div class="cbb">
<h3>Hall of Shame<img class="emblem" src="{$baseurl}/images/bananas/banana-g-bunch.png" alt="Green Bananas" /></h3>
<ol>
{foreach name=top from=$stats.bottom key=key item=member}
	{if $member.real == TRUE}        
		<li><a href="{$baseurl}/Community/Members/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{else}
		<li><a href="{$baseurl}/Community/Bananas/{$member.username}">{$member.username}</a> ({$member.sum})</li>
	{/if}
{/foreach}
</ol>
</div>
