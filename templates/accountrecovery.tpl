{include file="../static/fragments/Recovery.txt"}

<div class="box">
	<div class="boxhead">
		<h3>Account Recovery</h3>
	</div>
	<div class="boxcontent">
		{if $mode == 'auth'}
			<p>Please enter your <b>university</b> username and password</p>
			<form name ="userstuff" method ="POST" action = "{$url}">
				<input type="text" name="username" placeholder="Username">
				<br>
				<input type="password" name="password" placeholder="Password">
				<br>
				<input type="submit" name="Userauth" VALUE="Login">
			</form>
		{elseif $mode == 'resetpass'}
			<form name ="resetpass" method ="POST" action = "{$url}">
				<p>Your SUCS username is <b>{$usrname}</b></p>
				<p>Please enter the new password for your account:</p>
				<input type="password" name ="newpass" placeholder="Password">
				<p>Enter the password again:</p>
				<input type="password" name ="newpass2" placeholder="Password">
				<input type="submit" name="Resetpass" VALUE="Reset password">
			</form>
		{/if}
	</div>
<div class="hollowfoot"><div><div></div></div></div>
</div>