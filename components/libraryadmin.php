<?php

// you gotta be this high to enter
$permission="librarian";
//include ISBN validation library
require_once('../lib/Validate/ISPN.php');

$error = array();

function fetch_isbndb_data($isbn) {

	$url1 = "http://isbndb.com/api/books.xml?access_key=I6AH5WJI&index1=isbn&value1=";

	return file_get_contents($url1.$isbn);
}

function valid_isbn($isbn) {

        return (new Validate_ISPN)->isbn($isbn);
}

if ($session->groups[$permission]) {

	if ($_REQUEST['action'] == "search") {

		$isbn = $_REQUEST['isbn'];
		$isbn = str_replace(array('ISBN', '-', ' ', "\t", "\n"), '', $isbn);

		if (valid_isbn($isbn)) {
			$xmlresult = fetch_isbndb_data($isbn);
			$simple_xml = simplexml_load_string($xmlresult);
			$book['isbn'] = $isbn;
			$book['title'] = $simple_xml->BookList->BookData->Title;
			$book['author'] = $simple_xml->BookList->BookData->AuthorsText;
			if (substr(trim($book['author']),-1)==',') {$book['author']=substr(trim($book['author']),0,-1);}
			$book['publisher'] = $simple_xml->BookList->BookData->PublisherText;
			$book['image_url'] = ""; 
			$smarty->assign("book", $book);
		} else {
			// invalid isbn entered
			trigger_error("invalid ISBN number entered", E_USER_WARNING);
		}

		
	} elseif ($_REQUEST['action'] == "add") {
		$book = array();
		$book['isbn'] = $_REQUEST['isbn'];
		$book['title'] = $_REQUEST['title'];
		$book['author'] = $_REQUEST['author'];
		$book['publisher'] = $_REQUEST['publisher'];
		$book['image_url'] = $_REQUEST['image_url'];

		// Validate that we have enough info to add
		if (($book['isbn'] != "") && (!valid_isbn($book['isbn']))) {
			trigger_error("invalid ISBN", E_USER_WARNING);
		} elseif (trim($book['title']) == "") {
			trigger_error("you must supply a title", E_USER_WARNING);
		} elseif (trim($book['author']) == "") {
			trigger_error("you must supply an author"); 
		} else {
			$insertdata = array($book['title'], $book['author'], $book['publisher']);
			if (valid_isbn($book['isbn'])) {
				$book['amazon_data'] = fetch_isbndb_data($book['isbn']);
				$newinsertdata = array($book['isbn'], $book['image_url'], $book['amazon_data']);
				$insertdata = array_merge($insertdata, $newinsertdata);
				$DB->Query("INSERT INTO books (title, author, publisher, isbn, image_url, amazon_data) VALUES (?,?,?,?,?,?)", $insertdata);
			} else {
				$DB->Query("INSERT INTO books (title, author, publisher) VALUES (?,?,?)", $insertdata);
			}
		}

	}
		$result = $smarty->fetch("library-addbook.tpl");
}

$smarty->assign("title", "Library Admin");
$smarty->assign("body", $result);

?>
