<?php
/*
 * Main events page, including admin functions
 */


$permission = "sucsstaff";
$eventtable = "events";
$eventcategories = array("Technical","Talks","Gaming","Social","Misc");

$smarty->assign("event_categories", $eventcategories);

$events_index = array_search("Events", $pathlist);

if (isset($pathlist[$events_index + 1])) {
	list($eventcat, $eventid) = preg_split("/_/", $pathlist[$events_index + 1]);
	if (!in_array($eventcat, $eventcategories)) {
		trigger_error("Invalid category specified", E_USER_ERROR);
		unset($eventcat, $eventid);
	} elseif (!ctype_digit($eventid)) {
		trigger_error("Invalid event specified", E_USER_ERROR);
		print_r($eventid);
		unset($eventcat, $eventid);
	}
}

if (isset($session->groups[$permission])) {
	$smarty->assign("editable", true);
	$action = @$_REQUEST['action'];

	// process form actions with side-effects first
	switch ($action) {
		case "save":
			$record['name'] = $_REQUEST['name'];
			$record['description'] = $_REQUEST['description'];
			$record['location'] = $_REQUEST['location'];

			// reconstruct date/time
			$datetime = $_REQUEST['Date_Year'];
			$datetime .= str_pad((int) $_REQUEST['Date_Month'],2,'0',STR_PAD_LEFT);
			$datetime .= str_pad((int) $_REQUEST['Date_Day'],2,'0',STR_PAD_LEFT);

			$datetime .= " ".$_REQUEST['Time_Hour'];
			$datetime .= ":".$_REQUEST['Time_Minute'];


			$record['whn'] = $datetime;
			$record['category'] = $_REQUEST['category'];
			$id = @$_REQUEST['id'];

			// this may be an existing event which needs to be updated
			if (ctype_digit($id)) {
				$DB->AutoExecute($eventtable, $record, 'UPDATE', "id=".$id);
			} else {
				$DB->AutoExecute($eventtable, $record, 'INSERT');
			}

			unset($action);
			break;
    }
}

if (isset($session->groups[$permission]) && isset($action)) {
	switch($action) {
		case "create":
			$event = array("id" => "*");
			$body = $smarty->fetch("event_edit.tpl");
			break;
		case "edit":
			if (isset($eventcat) && isset($eventid)) {
				$event = $DB->GetRow("SELECT * FROM $eventtable WHERE id=?", array($eventid));

				//make tastier breadcrumbs
				$pathlist[$events_index + 1] = $event['name'];

				$smarty->assign("event", $event);
				$body = $smarty->fetch("event_edit.tpl");
			}
			break;
	}
} else {
	// not logged in, or no special action required
	if (isset($eventcat) && isset($eventid)) {
		$event = $DB->GetRow("SELECT * FROM $eventtable WHERE id=?", array($eventid));

		//make tastier breadcrumbs
		$pathlist[$events_index + 1] = $event['name'];

		$smarty->assign("event", $event);
		$body = $smarty->fetch("event.tpl");
	} else {
		$events = $DB->GetAll("SELECT *,date_part('epoch', whn) as whn_timestamp
					FROM $eventtable WHERE date_trunc('day', whn) >= date_trunc('day',NOW()) ORDER BY whn ASC");
		$oldevents = $DB->GetAll("SELECT *,date_part('epoch', whn) as whn_timestamp 
					FROM $eventtable WHERE date_trunc('day', whn) < date_trunc('day', NOW()) ORDER BY whn DESC LIMIT 3");
		$smarty->assign("events", $events);
		$smarty->assign("oldevents", $oldevents);
		$body = $smarty->fetch("events.tpl");
	}
}

$smarty->assign("body", $body);
$smarty->assign("title", "Events");
//$smarty->assign("secondary", file_get_contents("../static/fragments/Events.txt"));


?>
