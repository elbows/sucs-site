<?php

// we're potentially doing stuff with members here
include_once("../lib/members.php");

// who d'you have to be?
$permission = "librarian";
$librarian_mail = "librarian@sucs.org";

// don't try to convert existing html entities
// this could be broken out someplace else
function htmlentities2($myHTML) {
   $translation_table=get_html_translation_table (HTML_ENTITIES,ENT_QUOTES);
   $translation_table[chr(38)] = '&';
   return preg_replace("/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/","&amp;" , strtr($myHTML, $translation_table));
}

$library_index = array_search("Library", $pathlist);

if (isset($session->groups[$permission])) {
        $smarty->assign("librarian", TRUE);
}

// Default to browsing, empty search box, generic title
$mode = "browse";
$search = "";
$smarty->assign("title","Library");
$output2 = $smarty->fetch("library-search.tpl");
$smarty->assign("secondary", $output2);

if (isset($_REQUEST['search']) && (trim($_REQUEST['search']) != "")) {
// Pass the template some search results
	$mode = "search";
} elseif (isset($pathlist[$library_index +1]) && ($pathlist[$library_index + 1] == "Tags")) {
	if (isset($pathlist[$library_index + 2])) {
		$tag = $pathlist[$library_index + 2];
		// we're displaying books with a specific tag
		//The following line doesn't do what I think you think it does.
		//It will return a string if the tag exists, "1" and "0" when a tag doesn't exist
		$categories = $DB->GetOne("SELECT count(name) FROM bookcategories WHERE name=?", array($tag));
		//debug stuff, LOOK IT'S A STRING
		//var_dump($categories);
		//if ($categories['count'] == 1) {
		//$categories is not an aray, it's a string, yeah i know == != good_programing
		if ($categories == 1) {
			$mode = "tagdisplay";
			$query = "SELECT b.id, b.title FROM books AS b JOIN booktags AS bt ON b.id= bt.bookid"; 
			$query .= " JOIN bookcategories AS bc ON bc.id = bt.tag WHERE bc.name=?";
			$smarty->assign("results", $DB->GetAll($query, array($tag)));
		} else {
		// this tag doesn't exist...
			$mode = "tagerror";
		}
	} else {
		$mode = "taglist";
		$smarty->assign("tags", $DB->GetAll("SELECT name FROM bookcategories"));
	}

} elseif (isset($pathlist[$library_index + 1]) && is_numeric($pathlist[$library_index + 1])) {
// We're displaying a specific book
	$mode = "display";
	$checkout_request = false;
	$book_index = intval($pathlist[$library_index + 1]);
	if (isset($session->groups[$permission])) $smarty->assign('editable', true);

// Check this book actually exists
	$loans = $DB->GetAll("SELECT onloan FROM books WHERE id=? LIMIT 1", array($book_index));
	if (sizeof($loans) != 1) {
		$mode = "bookerror";
	} else {

	// See if we're supposed to be loaning/returning/editing/saving this book 
		if ($session->loggedin && isset($_REQUEST['action'])) {
			if ($_REQUEST['action'] == "loan") {
				if (isset($session->groups[$permission])) {
				// update DB
					$query = "UPDATE books SET onloan='t', loandate=now(), loanwho=? WHERE id=?";
					$DB->Query($query, array($_REQUEST['member'], $book_index));
				} else {
				// send mail to librarians
				// do this in a moment when we have more details about the book
					$checkout_request = true;
				}
			} elseif (($_REQUEST['action'] == "return") && isset($session->groups[$permission])) {
				// update DB
				$DB->Query("UPDATE books SET onloan='f', loandate=NULL WHERE id=?", array($book_index));
			} elseif (($_REQUEST['action'] == "edit") && isset($session->groups[$permission])) {
				// we're an editor and want to edit this book
				$smarty->assign("editing", true); 
			} elseif (($_REQUEST['action'] == "save") && isset($session->groups[$permission])) {
				// save edited book
				$book['title'] = $_REQUEST['title'];
				$book['author'] = $_REQUEST['author'];
				$book['publisher'] = $_REQUEST['publisher'];
				$book['description'] = $_REQUEST['description'];

				if ($DB->AutoExecute('books', $book, 'UPDATE', "id=".$DB->qstr($book_index))) { 
					message_flash_postponed("Book Updated!");
					//redirect to prevent form resubmission
					header('HTTP/1.1 303 See Other');
					header("Location: $baseurl$path"); 
				} else {
					trigger_error("Error updating book: ".$DB->ErrorMsg(), E_USER_WARNING);
				}

			}
		}

		$results = $DB->GetAll("SELECT * FROM books WHERE id=? LIMIT 1", array($book_index));
		$book = $results[0];

		if ($checkout_request) {
			// someone wants to check out this book
			$msgbody = "User {$session->username} would like to take out";
			$msgbody .= " {$book['title']} by {$book['author']} from the library.\n\n";
			$msgbody .= "Visit https://$preferred_hostname$path to process this request.";
			mail($librarian_mail, "Book Request", $msgbody);

			$smarty->assign("checkout_request", true);
		}

	// Clean up ready for output
		$book['title'] = htmlentities2($book['title']);
		$book['author'] = htmlentities2($book['author']);
		$book['onloan'] = ($book['onloan'] == 't') ? true : false;

		if (!isset($book['description'])) {
			// no book description in the database, try using Amazon data

			// Extract amazon data (maybe this should be stored in separate fields in the db?)
			$simple_xml = simplexml_load_string($book['amazon_data']);

			$book['description'] = @$simple_xml->Items->Item->EditorialReviews->EditorialReview->Content;
			if (isset($book['description'])) {
				// tidy description markup
				$tidy_config['doctype'] = 'omit';
				$tidy_config['output-xhtml'] = true;
				$tidy_config['show-body-only'] = true;
				$tidy_config['logical-emphasis'] = true;
				$book['description'] = tidy_repair_string($book['description'], $tidy_config);

				// Commented out by ~imranh
				// update db so we don't have to do this next time
				//$DB->Execute("UPDATE books SET description=? WHERE id=?", array($book['description'],$book['id']));
			}
		}

		$smarty->assign("book", $book);


	// Add loan interface to sidebar if we're logged in
		if ($session->loggedin) {
			$members = new Members;
			$smarty->assign("memberlist", $members->getMemberList());
			$secondary = $smarty->getTemplateVars("secondary");
			$secondary .= $smarty->fetch('library-loan.tpl');
			$smarty->assign("secondary", $secondary);
		}


	// Edit the path list to make the breadcrumbs tastier
		$pathlist[$library_index + 1] = $results[0]['title'];
	}
} else {
//Nothing being requested, just find some random books to put on main page

	$smarty->assign("tags", $DB->GetAll("SELECT name FROM bookcategories"));
	$smarty->assign("randoms", $DB->GetAll("SELECT * FROM books WHERE image_url IS NOT NULL ORDER BY random() LIMIT 4"));
}

if ($mode == "search") {
	$search = $_REQUEST['search'];
	$query = "SELECT id, title, onloan FROM books WHERE (title || ' ' || author || ' ' || keywords) ~* ? ORDER BY title ASC";
	$results = $DB->GetAll($query,array($search));

	foreach ($results as &$result) {
		$result['title'] = htmlentities2($result['title']);
		$result['onloan'] = ($result['onloan'] == 't') ? true : false;
	}
	
	$pathlist[] = "Search";
	$smarty->assign("results", $results);
} elseif ($mode == "display") {


}

$smarty->assign("mode", $mode);
$smarty->assign("search", $search);
$smarty->assign("pathlist", $pathlist);
$output = $smarty->fetch("library.tpl");
$smarty->assign("body",$output);

?>
