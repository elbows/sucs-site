<?php
$motd_file = "/etc/motd";

function decode_entities($text, $quote_style = ENT_COMPAT) {
   if (function_exists('html_entity_decode')) {
       $text = html_entity_decode($text, $quote_style, 'ISO-8859-1'); // NOTE: UTF-8 does not work!
   }
   else {
       $trans_tbl = get_html_translation_table(HTML_ENTITIES, $quote_style);
       $trans_tbl = array_flip($trans_tbl);
       $text = strtr($text, $trans_tbl);
   }
   $text = preg_replace('~&ndash\;~i', '-', $text);
   $text = preg_replace('~&#x([0-9a-f]+);~ei', 'chr(hexdec("\\1"))', $text);
   $text = preg_replace('~&#([0-9]+);~e', 'chr("\\1")', $text);
   return $text;
}

//function to count and collect hyperlinks
$links_count = 0;
function linkcounter($uri) {
	global $links_count, $uris;
	$uris[++$links_count] = $uri;
	return $links_count;
}

$poemmode = 0;

$news = $DB->GetAll("SELECT * FROM news WHERE sticky=true AND expiry > now() ORDER BY date DESC");

if (count($news)<3) {
  $news = $DB->GetAll("(SELECT * FROM news WHERE sticky=false AND expiry > now() ORDER by date DESC LIMIT ".(3-count($news)).") UNION SELECT * FROM news WHERE sticky=true AND expiry > now() ORDER BY date DESC");
}

if (count($news)<1) {
    $news = $DB->GetAll("SELECT * FROM poems ORDER BY RANDOM() LIMIT 1");
    $poemmode = 1;
}


$motd = "----------------------------- MESSAGE OF THE DAY ------------------------------\n\n";
for ($i=count($news)-1;$i>-1;$i--) {
$nitem = str_replace("\n", "", $news[$i]);
$nitem = str_replace("\r", "", $nitem);
$motd .= "  ".$nitem['title']."\n";
$body = str_replace("</p>", "\n\n    ", $nitem['body']);
$body = str_replace("<br />", "\n    ", $body);
$body = str_replace("<li>", "  * ", $body);
$body = str_replace("</li>", "\n    ", $body);
$body = str_replace("&nbsp;", " ", $body);
$body = str_replace("&hellip;", "...", $body);
$body = str_replace("&apos;", "'", $body);
// remove hyperlinks, collect to display later
$body = preg_replace(":<a href=['\"](.+?)['\"].*?>(.*?)</a>:e", "'\\2['.linkcounter('\\1').']'", $body);
$body = strip_tags($body);
$body = decode_entities($body);
$body = wordwrap($body, 75, "\n    ", 1);
$body = rtrim($body);
$motd .="    ".$body."\n";
if (isset($uris)) {
	$footer = "----\n";
	foreach ($uris as $urinum => $uri) {
			$footer .= "    [$urinum]: $uri\n";
			//remove uri from list so it won't show up on the next post
			unset($uris[$urinum]);
	}
	$motd .= "    ".$footer;
}
$motd .= str_pad($nitem['author'], 78, " ", STR_PAD_LEFT)."\n";

if ($poemmode == 1 && $nitem['submitter'] != '') {
	$motd .= str_pad("Submitted by ".$nitem['submitter'], 78, " ", STR_PAD_LEFT)."\n";
}
$motd .= "\n";
}
$motd .= "---------------------------- [ http://sucs.org/ ] -----------------------------\n";

//if (time()-filemtime($motd_file) < 86000  && $poemmode == 1) { //86000 to allow a little slack depending on when cron runs
    // print "MOTD too recent to replace with a poem!\n";
//} else {
    file_put_contents($motd_file, $motd);
//}
?>
