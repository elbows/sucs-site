<?php
	require_once("../lib/validation.php");
	//This script will allow the user to reset their password using their university credentials
	//2 modes, auth and resetpass which are sent to smarty so it can display the right form.
	//default state
	$mode = 'auth';
	include_once("../lib/ldap-auth.php");
	$smarty->assign("title", "Account Recovery");
	//Have they already started resetting?
	if(isset($session->data['recoveryuser']) && isset($_POST['newpass'])) {
		$ldifpath = '/tmp/passreset_' . $session->data['recoveryuser'] . '.ldif';
		if ($_POST['newpass'] !== $_POST['newpass2']){
			trigger_error("The passwords must match.", E_USER_WARNING);
		}
		elseif (weakPassword($_POST['newpass'])){
			trigger_error("Your password is too weak!", E_USER_WARNING);
			unset($newpass);
		}
		else{
			//Reset their password
			$hashpass = base64_encode(sha1($_POST['newpass'], true));
			$ldif = "dn: uid=" . $session->data['recoveryuser']. ",ou=People,dc=sucs,dc=org
changetype: modify
replace: userPassword
userPassword: {SHA}$hashpass";

			file_put_contents($ldifpath, $ldif);
			//for now specify the full command, would be nicer to have a shell script for this instead.
			system("ldapmodify -x -H ldap://silver -D'cn=Manager,dc=sucs,dc=org' -y /etc/ldap.secret -f " . $ldifpath);
			unlink($ldifpath);
			unset($session->data['recoveryuser']);
			message_flash("Your password has been successfully changed.");
		}
	}
	else{
		$mode = 'auth';
		//if they have tried to log in, try and auth them
		if (isset($_POST['username'])) $authd = ldapAuth($_POST['username'], $_POST['password']);
		//auth failed, tell them they got something wrong
		if ($authd == "nope") {
			trigger_error("Bad username or password", E_USER_WARNING);
		}
		elseif ($authd == "uni"){
			//if they are authd, try and get their username
			$usrname = $sucsDB->GetOne('SELECT username FROM members WHERE sid=?', $_POST['username']);
			//check if they are a member of sucs
			if($usrname !== ""){
				$session->data["recoveryuser"] = $usrname;
				$mode = 'resetpass';
			}
			else{
				header('Location: http://www.swansea-union.co.uk/mysociety/sucs/');
			}
		}
	}
	//Things to make smarty work
	$smarty->assign("mode", $mode);
	$smarty->assign("usrname", $usrname);
	$output=$smarty->fetch("accountrecovery.tpl");
	$smarty->assign("body", $output);

?>
