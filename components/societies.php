<?php
// Constants
define('_BASE_DN', 'ou=People,dc=sucs,dc=org');
define('_LDAP_SERVER', 'ldap://silver');

// Connect and bind to ldap server
$conn = ldap_connect(_LDAP_SERVER);
# ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);
# $bind = ldap_bind($conn);

$search = ldap_search($conn, _BASE_DN, 'uid=*', array('uid', 'cn', 'homedirectory'));

        // Sort By Username
        ldap_sort($conn, $search, 'uid');
        // Produce an array of usernames
        $usernames = array();
        $entryHandler = ldap_first_entry($conn, $search);
        while($entryHandler) {
                $username = ldap_get_values($conn, $entryHandler, 'uid');
                $fullname = ldap_get_values($conn, $entryHandler, 'cn');
		$homedir  = ldap_get_values($conn, $entryHandler, 'homedirectory');

		$homedir = $homedir[0];
		$homedirArray  = explode('/', $homedir);

			if (($homedirArray[2]=="society") && file_exists( "$homedir/public_html")) { 
				$usernames[] = array( "username" => $username[0], "fullname" => $fullname[0]);
			}
                $entryHandler = ldap_next_entry($conn, $entryHandler);
        }

$smarty->assign("societies", $usernames);


$output = $smarty->fetch("societies.tpl");
$smarty->assign("title", "Societies");
$smarty->assign("body", $output);

?>
