<h2>Terms and Conditions of a Swansea University Computer Society (SUCS) account</h2>

<p>Version: 1.2<br />
Date: May 2002</p>

<p><strong>NOTE:</strong> Nothing in this document constrains similar Regulations for the Use of Computing Facilities of the University of Wales Swansea.</p>

<ol>
<li>Members of Swansea University Computer Society are entitled to an account on the SUCS network. For all except life members, membership will only be granted on payment of the appropriate fee.</li>
<li>All members agree not to do anything to bring the Society into disrepute, and to abide by the Terms and Conditions of the Society, as noted below and as may be subsequently amended. Non-compliance with these rules may result in membership being withdrawn.</li>
<li>Members must comply with the regulations of the network(s) over which they access the SUCS network. This will always include the UK Joint Academic Network (JANET), the Acceptable Use Policy of which can be found at <a href="http://www.ja.net/services/publications/policy/aup.html">http://www.ja.net/services/publications/policy/aup.html</a>.</li>
<li>Except when explicitly set up for clubs or groups within the University, accounts on the SUCS network are strictly for use by one person only. Multiple use of personal accounts is prohibited, and may result in the offending owner's account being withdrawn. 'Multiple use' includes the owner of the account making known his/her password to others, as well as allowing others access to the account by other means.</li>
<li>Members must take into account the relevant laws of England and Wales as they apply to their use of the SUCS network. These include, but are not limited to, the Data Protection Act (1984), the Copyright, Designs and Patents Act (1988), the Obscene Publications Act (1959 &amp; 1964 etc), the Telecommunications Act (1984), and the Computer Misuse Act (1990).</li>
<li>Members must always use the SUCS network in a manner that does not adversely affect its use by others. In particular, disproportionate use of system resources (including, but not limited to, disc space, network bandwidth and processor time), may result in offending processes and/or files being terminated and/or removed without warning.</li>
<li>Members must ensure that they are authorised to use any services and machines to which they may connect from the SUCS network. Use of the SUCS network to gain unauthorised access to other computers is forbidden.</li>
<li>Members must not create, store or transmit any information on the SUCS network that is:
<ol class="roman">
	<li>in breach of current copyright laws. It is the member's responsibility to ensure that all files stored in their account meet this criterion. This applies equally to graphics, audio, video and text files, whether copied from other sources or scanned from books and other publications. It applies to files viewable only by other SUCS members and to those publically viewable (e.g. via web pages);</li>
	<li>obscene, indecent, or defamatory, or capable of being resolved into such files;</li>
	<li>commercial in nature. This criterion also prohibits the sending of commercial e-mail, whether unsolicited (UCE/'spam') or solicited, as well as transmission of commercial material via web pages;</li>
	<li>malicious in intent, whether against other people or other computer systems.</li>
</ol>
</li>
<li>The Member agrees that SUCS may charge him/her for any reasonable costs incurred as a result of their breach of Rule 5 above. The Member further agrees to indemnify SUCS against any action that may be taken against it as a result of the member's breach of Rules 6, 7 and 8 above.</li>
<li>It is the user's responsibility to control access permissions to their files. Except where a breach of the rules above has occurred, or in similar exceptional circumstances, SUCS system administrators will not examine a Member's private files without prior permission from the Member.</li>
<li>Members will not have their account withdrawn unless deemed necessary by the SUCS systems administrators and/or committee.</li>
<li>Members will be held accountable for the content or abuse of their account unless it can be shown that it was without their consent or prior knowledge.</li>
<li>Use of the Room and its facilities is conditional on any additional rules posted in the room, and on official notices.</li>
<li>These rules are subject to change by the committee as defined in the constitution. A copy of all current rules and conditions can be found on official notice boards, or can be obtained from the society secretary.</li>
<li>Members are not permitted to run any servers or unattended services, including bots, game servers, distributed computing clients or otherwise without the explicit written authorisation of the admin team.</li>
</ol>

<h4>Definitions:</h4>
<ol class="roman">
<li>'SUCS': Swansea University Computer Society</li>
<li>'the SUCS network': any machine fully or partly run by SUCS. This includes machines to which a connection may be made from the rest of the Internet, as well as those only accessible by first making an authorised connection to such a machine.</li>
<li>'systems administrators': any member of SUCS having administrator (root, or similar) access to any machine on the SUCS network.</li>
<li>'committee': the current committee of SUCS, as defined in the SUCS Constitution, and any individuals acting with the Committee's consent on their behalf.</li>
<li>'The Room': The computing room designated to, and run by, the society, and all its contents.</li>
</ol>
