



<p>One very handy service offered by SUCS is &quot;Desktop on Demand&quot;. This is a method of using an X desktop session in a Java-enabled web browser, enabling you to use one of the SUCS workstations from anywhere in the world. It even works from BT Internet payphones!</p>

<p>You may wonder what&#39;s so wonderful about this, given that you&#39;d have to be at a computer to use it. Sometimes, you may be at a computer that doesn&#39;t have the software you want to use installed or is locked down in some way (the payphone being a good example). By using Desktop on Demand, you can bypass these restrictions and use any software that&#39;s available in the SUCS room. Bear in mind, however, that the image data for the desktop is coming over the Internet and so it&#39;s going to be slower than if you were at the machine - you&#39;re not going to be able to play BZFlag this way!</p>

<h2>Getting started</h2>
<p>To use Desktop on Demand, go to <a href="../../../Tools/Desktop%20on%20Demand">https://sucs.org/Tools/Desktop on Demand</a> and choose a resolution to suit your seen size and connection. You should then get a screen that looks something like this:<br /><img src="../../../pictures/desktoplogin.jpg" alt="" /><br />Put your SUCS username and password in, pressing enter after each and you will be logged in. Once the desktop has finished loading, you should end up with a screen that looks something like this:<br /><img src="../../../pictures/desktopdesktop.jpg" alt="" /><br />You can now run applications by selecting them from the Applications menu, and you will find the files in your SUCS disk space in the &quot;<span style="font-style: italic">user</span>&#39;s Home&quot; folder on the desktop.<br /></p>
