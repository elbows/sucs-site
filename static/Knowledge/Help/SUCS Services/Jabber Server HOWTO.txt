<h2>What is Jabber?</h2>
<p>Jabber is an instant messenging system much like MSN, AIM or Yahoo! Messenger. However, unlike these proprietary systems, Jabber is an open specification which allows anyone to run their own server. Jabber is designed to allow communication across different servers, so you can talk to Jabber users on different servers to your own.</p>
<p>SUCS runs a Jabber server, and this guide will show you how to use it.</p>
<h2>Using Jabber</h2>
<h3>Requirements</h3>
<ul>
<li>A Jabber client. Any Jabber client will do, though the popular choices for Windows and Linux users are <a href="http://pidgin.im">Pidgin</a>&nbsp;or <a href="http://psi.sf.net">Psi</a>, and Mac OS X users can use <a href="http://www.adiumx.com/">Adium</a>.</li>
<li>A SUCS membership. If you're not yet a member, <a href="../../../join">join today</a>!</li>
<li>An internet connection. If you're reading this then the chances are you fulfil this requirement already.</li>
</ul>
<h3>Adding your Account to your client.</h3>
<p>In Pidgin, click 'Accounts' from the 'Tools' menu, and then click the button labelled 'Add'. You'll be presented with a window which you should fill in a similar fashion to the following (your Jabber 'Screen Name' should be the same as your SUCS username, and the password, your SUCS password):</p>
<p>
<img src="../../../pictures/jabber1.png" /></p>
<p>Click 'save' at this point to add your user.<br />The resource box can be pretty much anything you like - note that you can be signed in from multiple places, as long as the resource field is different for each one. Logging in to the server will sign out any other clients you have running using the same resource, which can be useful.</p>
<p>To actually connect to the Jabber server, tick the 'Online' box in the Pidgin accounts window. If 'Auto-login' is ticked, you'll be logged in to the Jabber server every time you start Pidgin.</p>
<h3>Adding Buddies</h3>
<p>You'll now want to fill up your buddy list (or 'roster') with people to talk to, so select 'Add Buddy' from the 'Buddies' menu, and enter the buddy's details in the new window:</p>
<p>
<img src="../../../pictures/jabber3.png" /></p>
<p>This will send an authorisation request to your buddy, who will have to accept it before he/she'll appear on your buddy list.</p>
<p>Once you've got people on your buddy list, just double-click their entry on the list to start chatting.</p>