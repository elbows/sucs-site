<?php

function smarty_function_getparams($params, &$smarty) {

	$output = "";

	if (isset($params['gets'])) {
		$output .= "?";
		$i = 0;
		foreach ($params['gets'] as $name => $value) {
			if ((trim($name) == "") || (trim($value) == ""))
				continue;

			if ($i != 0) {
				$output .= "&amp;";
			} else {
				$i++;
			}
			$output .= "$name="."$value";
		}
	}

	return $output;
}

?>
